//
//  LinkPreviewTests.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 31.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import XCTest
@testable import HipChatParser

class LinkPreviewTests: XCTestCase {
    
    var testData:[[String:Any]]! = TestDataLoader().testDataFromPlist(named: "TestLinkPreviewData")

    // MARK: - Unit Tests
    
    func testMultipleTitleDetection(){
        for item in self.testData{
            if let html = item["html"] as? String {
                let testTitle = item["title"] as? String
                let detectedTitle = LinkPreview.getTitle(fromHTML: html)
                XCTAssertEqual(testTitle, detectedTitle, "Error detecting 'title' in Test-Data HTML:\n\(html)")
            }else{
                XCTFail("Test failed due to malformed test-data: item[\"html\"] couldn´t be loaded")
            }
        }
    }
    
    func testGetTitleFail(){
        let noTitle = LinkPreview.getTitle(fromHTML: "<html><head><notitle>notitle</notitle><head></html>")
        XCTAssertNil(noTitle, "Title should be nil if not title-tag was found.")
        
        let emptyTitle = LinkPreview.getTitle(fromHTML: "<html><title></title></html>")
        XCTAssertNil(emptyTitle, "Title should nil if title-tag without value was found.")
        
        let emptyTwitterContent = LinkPreview.getTitle(fromHTML: "<html><head><meta name=\"twitter:title\" content=\"\"></head></html>")
        XCTAssertNil(emptyTwitterContent, "Title should nil ''-value (empty string) was found.")
        
        let emptyOpenGraphContent = LinkPreview.getTitle(fromHTML: "<html><head><meta property=\"og:title\" content=\"\"></head></html>")
        XCTAssertNil(emptyOpenGraphContent, "Title should nil if ''-value (empty string) was found.")

        let emptyMetaTitleContent = LinkPreview.getTitle(fromHTML: "<html><head><meta name=\"title\" content=\"\"></head></html>")
        XCTAssertNil(emptyMetaTitleContent, "Title should nil if ''-value (empty string) was found.")
        
        XCTAssertNil(LinkPreview.getTitle(fromHTML: ""), "Should return nil for empty-HTML-String")
    }
    
    func testComputedURL(){
        let urlString = "www.twitter.com"
        let testURL = URL(string:"http://www.twitter.com")
        
        if let linkUrl = LinkPreview.mocked(url: urlString, title: "Home").linkURL{
            XCTAssertEqual(testURL, linkUrl)
        }else{
            XCTFail("Test failed! ")
        }
    }
    
    func testURLGeneratingFail(){
        let linkPreview = LinkPreview.mocked(url: "", title: "")
        
        XCTAssertNil(linkPreview.linkURL, "Test Failed: LinkPreview.linkURL should be 'nil' if passed String is no URL-String")
    }
    
    // MARK: - Integration Tests
    
    func testTitleDetectionWithSceme(){
        
        let testLink = LinkPreview(linkString: "http://www.nbcolympics.com")
        
        XCTAssertEqual(testLink.title, "2018 PyeongChang Olympic Games | NBC Olympics", "Website Title couldn´t be detected for \(testLink.url)")
    }
    
    func testTitleDetectionWithoutSceme(){
        let testLink = LinkPreview(linkString: "www.nbcolympics.com")
        
        XCTAssertEqual(testLink.title, "2018 PyeongChang Olympic Games | NBC Olympics", "Website Title couldn´t be detected for \(testLink.url)")
    }
    
    func testTitleDetectionFail(){
        
        let testLink = LinkPreview(linkString: "http://twitter")
        XCTAssertNil(testLink.title, "Even though the string could be detected as link, the 'title' should be nil. Due to the missing top-level domain in url-string, the html couldn´t be loaded")
    }
}
