//
//  HipChatParserTests.swift
//  HipChatParserTests
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import XCTest
@testable import HipChatParser

class MessageComponentsTests: XCTestCase {
    
    lazy var testData:[[String:Any]] = TestDataLoader().testDataFromPlist(named: "TestRawMessages")
    
    func testEmoticonsDetection() {
        
        for item:[String:Any] in testData{
            
            let message = messageComponents(forTestItem: item)
            
            XCTAssertEqual(message.emoticons ?? [], item["emoticons"] as! Array, "Test for emoticons detection failed due to mismatching regex results for message: \(item["rawValue"])")
        }
    }
    
    func testMentionsDetection() {
        
        for item:[String:Any] in testData{
            
            let message = messageComponents(forTestItem: item)
            
            XCTAssertEqual(message.mentions ?? [], item["mentions"] as! Array, "Test for mentions detection failed due to mismatching regex results for message: \(item["rawValue"])")
        }
    }
    
    func testLinksDetection(){
        
        for item:[String:Any] in testData{
            
            let message = messageComponents(forTestItem: item)
            let messageURLs:[String]? = message.links?.map{ $0.url }
            XCTAssertEqual(messageURLs ?? [], item["links"] as! Array, "Test for links detection failed due to mismatching regex results for message: \(item["rawValue"])")
        }
    }
    
    // MARK: - Helper methods
    
     func messageComponents(forTestItem item: [String:Any]) -> MessageComponents{
        
        let message = MessageComponents(message:item["rawValue"] as! String)
        
        return message
    }
    
}
