//
//  SampleDataHelper.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 30.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import XCTest
@testable import HipChatParser

class SampleDataHelperTests: XCTestCase {

    func testLoadingEmoticons(){
        
        let emoticonStrings = SampleDataHelper.loadEmoticons()
       
        XCTAssertNotNil(emoticonStrings, "Test failed: loadEmoticons() shouldn´t return nil")
       
        XCTAssert(emoticonStrings!.count > 0, "Test failed: loadEmoticons() should return at least one emoticon string")
    }
 
    func testLoadingMentions(){
        
        let mentionStrings = SampleDataHelper.loadMentions()
       
        XCTAssertNotNil(mentionStrings, "Test failed: lloadMentions() shouldn´t return nil")
       
        XCTAssert(mentionStrings!.count > 0, "Test failed: loadMentions() should return at least one mention string")
    }
    
    func testLoadingLinks(){
        
        let linkStrings = SampleDataHelper.loadLinks()
        
        XCTAssertNotNil(linkStrings, "Test failed: loadLinks() shouldn´t return nil")
       
        XCTAssert(linkStrings!.count > 0, "Test failed: loadLinks() should return at least one link-string")
    }
    
    func testLoadingMessages(){
        
        let mesageStrings = SampleDataHelper.loadMessages()
        
        XCTAssertNotNil(mesageStrings, "Test failed: loadMessages() shouldn´t return nil")
        
        XCTAssert(mesageStrings!.count > 0, "Test failed: loadMessages() should return at least one message string ")
    }
    
    func testLoadDataFail(){

        let testData = SampleDataHelper.loadSampleData(for: .none)
        
        XCTAssertNil(testData, "Test Failed:  loadSampleData(for: .none) is expected to return nil")
    }
}
