//
//  TestDataLoadingHelper.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 31.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation

class TestDataLoader:AnyObject{
    
    func testDataFromPlist(named plistName: String) -> [[String:Any]] {
        let bundle = Bundle(for: type(of:self))
        
        guard let path = bundle.path(forResource: plistName, ofType: "plist"),
            let testData = NSArray(contentsOfFile: path) as? [[String:Any]] else {
                fatalError()
        }
        return testData
    }
}
