//
//  StringParserTests.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 02.01.17.
//  Copyright © 2017 Matthias Gebhardt. All rights reserved.
//

import XCTest

@testable import HipChatParser

class StringParserTests: XCTestCase {
    
    func testMatchesEmoticons(){
        
        let testMessage = "(qwe) (123) (abc) (aus)"
        
        let matches = StringParser.matches(in: testMessage, for: MessageComponentsRegexPattern.emoticon.rawValue, withCaptureGroupIndex: 1)

        XCTAssertEqual("qwe 123 abc aus", matches?.joined(separator: " "))
    }
    
    func testMatchesMentions(){
        
        let testMessage = "@bob @john @chris @lisa"
        
        let matches = StringParser.matches(in: testMessage, for: MessageComponentsRegexPattern.mention.rawValue, withCaptureGroupIndex: 1)
        
        XCTAssertEqual("bob john chris lisa", matches?.joined(separator: " "))
        
    }
    
    func testMatchesLinks(){
        
        let testMessage = "Check out http://twitter/ They go nuts about www.jira.com.au and bitbuket.com"
        
        let matches = StringParser.matches(in: testMessage, for: .link )
        
        XCTAssertEqual("http://twitter/ www.jira.com.au bitbuket.com", matches?.joined(separator: " "))
    }

    
    func testMatchesWithInvalidRegexFail(){
        
        let testMessage = "(qwe) @bob http://twitter.com"
        let invalidRegex = "(\\)"
        
        let matches:[String]? = StringParser.matches(in: testMessage, for: invalidRegex, withCaptureGroupIndex: 1)
        XCTAssertNil(matches, "Test Failed: Matches should be nil due to invalid regex")
        
    }
    
    func testMatchesWithInvalidCaptureGroupFail(){
        let matches = StringParser.matches(in: "@bob", for:  MessageComponentsRegexPattern.mention.rawValue, withCaptureGroupIndex: 2)
        XCTAssertEqual(matches!, [], "Test Failed: Matches should neither be non-empty, nor 'nil'. ")
    }
}
