//
//  JSONSerializationTests.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 30.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import XCTest
@testable import HipChatParser

class JSONSerializationTests: XCTestCase {

    func testLinkSerialization(){
        
        let testMessage = MessageComponents.mocked(links: [LinkPreview.mocked(url: "http://twitter", title:""),
                                                    LinkPreview.mocked(url: "http://google.com", title:"Google"),
                                                    LinkPreview.mocked(url: "http://apple.com", title:"Apple")])
        
        let testJSON = "{\"links\":[{\"url\":\"http://twitter\",\"title\":\"\"},{\"url\":\"http://google.com\",\"title\":\"Google\"},{\"url\":\"http://apple.com\",\"title\":\"Apple\"}]}"
        
        let jsonString:String = testMessage.toJSON()!
        
        XCTAssertEqual(jsonString, testJSON)
    }
    
    func testEmoticonSerialization(){
        
        let testMessage = MessageComponents.mocked(emoticons: ["abc", "123", "def", "success"])
        
        let testJSON = "{\"emoticons\":[\"abc\",\"123\",\"def\",\"success\"]}"
        
        let jsonString:String = testMessage.toJSON()!
        
        XCTAssertEqual(jsonString, testJSON)
    }
    
    func testMentionsSerialization(){
        
        let testMessage = MessageComponents.mocked(mentions: ["bob", "charly", "rafa", "rough"] )
        
        let testJSON = "{\"mentions\":[\"bob\",\"charly\",\"rafa\",\"rough\"]}"
        
        let jsonString:String = testMessage.toJSON()!
        XCTAssertEqual(jsonString, testJSON)
    }
    
    func testMessageSerialization(){
        let mockedTestMessage = MessageComponents.mocked(mentions:["bob", "charly"],
                                                  emoticons:["abc","123","success"],
                                                  links:[LinkPreview.mocked(url:"http://www.nbcolympics.com", title:"2016 Rio Olympic Games | NBC Olympics"),
                                                         LinkPreview.mocked(url:"http://google.com", title:"Google")])
        
        let testJSON = "{\"emoticons\":[\"abc\",\"123\",\"success\"],\"mentions\":[\"bob\",\"charly\"],\"links\":[{\"url\":\"http://www.nbcolympics.com\",\"title\":\"2016 Rio Olympic Games | NBC Olympics\"},{\"url\":\"http://google.com\",\"title\":\"Google\"}]}"
        
        let jsonString:String = mockedTestMessage.toJSON()!
        XCTAssertEqual(jsonString, testJSON)
    }
    
    
    func testMessageSerializationAsychronously(){
        
        let expectation = self.expectation(description: "Expecting to serialize Message-String into JSON-String asynchronously ")
        
        let operationQueue = OperationQueue()
        let transformOp = MessageStringToJSONTransformOperation(input:"@bob (yeah) http://www.nbcolympics.com")
        
        transformOp.completionBlock = {
            
            DispatchQueue.main.async {
                
                if let _ = transformOp.outputJSONString{
                    expectation.fulfill()
                }else{
                    XCTFail()
                }
            }
        }
        
        operationQueue.addOperation(transformOp)
        
        waitForExpectations(timeout: 30, handler: nil)

    }
    
}
