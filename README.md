# README #

### HipChatParser - TestApp ###

* HipChatParser is a small iOS application that takes a message-string and returns a JSON-String with all detected message components. It provides some convenience functionality to easily add sample messages or pre-formatted message components like:
    - mentions: e.g. @matthiasgeb
    - emoticons: e.g. (hipchat)
    - links: http://somelink.com
* for each detected link, the title of the corresponding webpage will be detected too 
* due to the download-process of the HTML, a network connection is nessascary to get the title, otherwise the title detection fails.

![HCP_Screenshot.png](https://bitbucket.org/repo/bGxBp5/images/1443897064-HCP_Screenshot.png)

**Project Setup**

* Xcode 8.2
* BaseSDK: iOS 10.2
* Deployment Target: iOS 9.3

**Dependencies:** 

* Kanna via CocoaPods (2.1.0) --> XML/HTML-Parser
    - based on libxml2
    - supports xpath expression 
    - used for HTML-title detection

Everything else should be self-explanatory. 

**Have fun! ;) **