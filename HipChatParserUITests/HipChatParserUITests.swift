//
//  HipChatParserUITests.swift
//  HipChatParserUITests
//
//  Created by Matthias Gebhardt on 02.01.17.
//  Copyright © 2017 Matthias Gebhardt. All rights reserved.
//

import XCTest

class HipChatParserUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    let inputTextView = XCUIApplication().textViews.element(boundBy: 0)
    let outputTextView = XCUIApplication().textViews.element(boundBy: 1)

    
    let randamMessageButton = XCUIApplication().navigationBars.element.buttons["Refresh"]
    let convertButton = XCUIApplication().buttons["▼ Transform to JSON ▼"]
    let resetButton = XCUIApplication().buttons["Reset"]
    let linkButton = XCUIApplication().buttons["http://link.com"]
    let mentionButton = XCUIApplication().buttons["@mention"]
    let emoticonButton = XCUIApplication().buttons["(emoticon)"]
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialRandomInputText() {
        
        let text = inputTextView.value as! String
        
        XCTAssert(!text.isEmpty, "Should not be empty")
        
    }
    
    func testRandomMessageButton() {
        
        randamMessageButton.tap()

        testInitialRandomInputText()
        
    }
    
    func testAddEmoticonButton() {
        
        let textBefore = inputTextView.value as! String
        
        emoticonButton.tap()

        let textAfter = inputTextView.value as! String

        XCTAssert(textBefore.characters.count < textAfter.characters.count, "InputText should be longer than before")
    }
    
    
    func testAddMentionButton() {
        
        let textBefore = inputTextView.value as! String
        
        mentionButton.tap()
        
        let textAfter = inputTextView.value as! String
        
        XCTAssert(textBefore.characters.count < textAfter.characters.count, "InputText should be longer than before")
    }
    
    func testAddLinkButton() {
        
        let textBefore = inputTextView.value as! String
        
        linkButton.tap()
        
        let textAfter = inputTextView.value as! String
        
        XCTAssert(textBefore.characters.count < textAfter.characters.count, "InputText should be longer than before")
        
    }
    
    func testResetButton() {
        
        resetButton.tap()
        
        let input = inputTextView.value as! String
        let output = inputTextView.value as! String
        
        XCTAssert(convertButton.isEnabled)
        XCTAssert(input.isEmpty && output.isEmpty, "InputText should be empty")
    }
    
    func  testConvertButton(){
        
        // Reset outputText, just to be sure there is no text yet
        resetButton.tap()
        XCTAssert((outputTextView.value as! String).isEmpty)

        // Set some sample text
        mentionButton.tap()
        emoticonButton.tap()
        XCTAssert(!(inputTextView.value as! String).isEmpty)
        
        convertButton.tap()
        
        // wait for conversion finishing, should be fast due to the lack of links
        sleep(1)
        
        XCTAssert(!(outputTextView.value as! String).isEmpty)
        XCTAssert(!(inputTextView.value as! String).isEmpty)
        
    }
    
    func  testResetButtonWhileConversion(){
        
        // Reset outputText, just to be sure there is no text yet
        resetButton.tap()
        XCTAssert((outputTextView.value as! String).isEmpty)
        
        // insert some links to increase time for conversion
        for _ in 0..<10{
            linkButton.tap()
        }
        
        XCTAssert(!(inputTextView.value as! String).isEmpty)
        
        convertButton.tap()

        XCTAssertFalse(convertButton.isEnabled)
        
        let spinner = app.activityIndicators.element
        XCTAssert(spinner.frame.height != 0 && spinner.frame.width != 0)
        
        testResetButton()

        // there must be a funny bug with iOS 9.x, it fails even though I can actually see it's not failing
        if #available(iOS 10.0, *) {
            XCTAssertFalse(spinner.exists, "Spinner shouldn't exist (isHidden=true) after reset button was tapped.")
        }
    }
}
