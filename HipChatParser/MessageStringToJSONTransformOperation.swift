//
//  StringToJSONStringOperation.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 29.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation

class MessageStringToJSONTransformOperation : Operation{
    
    var inputString:String
    var outputJSONString:String? = nil
    
    init(input:String) {
        self.inputString = input
    }
    
    override func main() {
        
        let message = MessageComponents(message:inputString)
        
        if let json = message.toJSON(options: .prettyPrinted ){
            outputJSONString = json
        }
    }
}
