//
//  AppDelegate.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

