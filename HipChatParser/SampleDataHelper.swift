//
//  TestdataHelper.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 29.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation

typealias SampleMessage = String
typealias SampleMention = String
typealias SampleEmoticon = String
typealias SampleLink = String



/// Helps by providing convenience function to get sample data from plist-files
class SampleDataHelper {
    
    enum PlistName:String{
        case SampleMessages
        case SampleMentions
        case SampleEmoticons
        case SampleLinks
        case none
    }
    
    static func loadEmoticons() -> [SampleEmoticon]?{
        
        return SampleDataHelper.loadSampleData(for: .SampleEmoticons)
    }
    
    static func loadLinks() -> [SampleLink]?{
        
        return SampleDataHelper.loadSampleData(for: .SampleLinks)
    }
    
    static func loadMentions() -> [SampleMention]?{
        
        return SampleDataHelper.loadSampleData(for: .SampleMentions)
    }
    
    static func loadMessages() -> [SampleMessage]?{
        
        return SampleDataHelper.loadSampleData(for: .SampleMessages)
        
    }

    static func loadSampleData(for plistName: PlistName) -> [String]?{
        
        guard let path = Bundle.main.path(forResource: plistName.rawValue, ofType: "plist"),
            let testData = NSArray(contentsOfFile: path) as? [String] else { return nil }
        
        return testData
    }
}
