//
//  Link.swift
//  HipChatTestApp
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation
import Kanna

/// LinkPreview value which will be initialized synchronously with two properties
struct LinkPreview:JSONSerializable {
    
    /// Detected Website-Link-URL-String within Message. It doesnt necessarily need to be a valid URL-String.
    /// Sample values:
    /// - "google.com"
    /// - "www.twitter.com"
    /// - "http://twitter"
    let url:String
    
    /// Detected HTML-Title-String from downloaded Website.
    /// Can only be detected if any network is reachable.
    /// default value = nil
    var title:String? = nil
    
    /// Computed URL based on `linkString`. If 'http'-scheme is missing, it will be added.
    var linkURL:URL?{
        get{
            guard let linkURL = URL(string: url) else {return nil}
            if linkURL.scheme == nil{
                return URL(string: "http://" + url)
            }
            return linkURL
        }
    }
    
    
    /// Creates a new `LinkPreview`-Object by passing in the detected Website-Link-String from e.g. chat-message-string.
    ///
    /// Due to synrchonous download of the Website-HTML for HTML-components detection, it should be considered to create a LinkPreview asynchroniously
    ///
    /// - Parameter linkString: Detected Website-Link-String.
    ///   It doesnt necessarily need to be a valid URL-String. 
    ///
    ///     Sample values:
    ///     - "google.com"
    ///     - "www.twitter.com"
    ///     - "http://twitter"
   init(linkString: String){
    
        self.url = linkString

        if let url = linkURL, let html = String.from(url: url, encoding: .utf8){
            self.title = LinkPreview.getTitle(fromHTML: html)
        }
    }

    /// Creates a new `LinkPreview`-Object by passing in the `url:String` and `title:String` directly.
    /// Mainly used to create mocked `LinkPreview`-Objects for testing
    ///
    /// - Parameters:
    ///   - url: Website-Link-URL-String which doesnt necessarily need to be a valid URL-String.
    ///   - title: HTML-Title-String for corresponding Website-URL-String
    fileprivate init(url:String, title:String){
        self.url = url
        self.title = title
    }
}

extension LinkPreview{
    
    /// Helper Method for HTML-`title` detection based on the given `html:String`.
    /// HTML-String will be parsed by xpath-descriptions for :
    /// - '\<title>'-Tags
    /// - '\<meta>'-Contents and specific attributes
    ///     - Title (name='title')
    ///     - OpenGraph (property='og:title')
    ///     - Twitter (name='twitter:title')
    ///
    /// - Parameter url: URL for WebSite to be downloaded
    /// - Returns: Downloaded HTML-String if URL points to website. There is no HTML-Validation.
    static func getHTML(fromURL url:URL, encoding: String.Encoding) -> String?{
        
        return String.from(url: url, encoding: encoding)
    }

    /// Helper Method for HTML-`title` detection based on the given `html:String`.
    /// - Parameter html: HTML-String - A valid (X)HTML-format increases chances for `title`-detection.
    /// - Returns:  Title-String if present and not empty. String will already be :
    ///     - html-decoded (no HTML-Entities present)
    ///     - trimmed (whitespaces and newlines removed)
    ///     - truncated (trancation will be replaced by "…")
    static func getTitle(fromHTML html:String) -> String?{
        
        guard let doc = HTML(html: html, encoding: .utf8) else { return nil }
        
        /// Collection of XPath Pattern for HTML-title detection.
        /// Detection will be performed prioritized in order of array-items.
        let titleDetectionPattern: [String] = [
            "/html/head/title",                               // e.g. <title>Home</title>
            "/html/head/meta[@name='title']/@content",        // e.g. <meta name="title" content="Home">
            "/html/head/meta[@property='og:title']/@content", // e.g. <meta property="og:title" content="Home">
            "/html/head/meta[@name='twitter:title']/@content" // e.g. <meta name="twitter:title" content="Home">
        ]

        for xpath in titleDetectionPattern{
            
            guard let title = doc.xpath(xpath).first?.text, !title.isEmpty else {continue}
            
            return title.htmlDecodedString()
                        .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        .truncateCharacters(after: 50, with: "…")
            
        }
        
        return  nil
    }
        
    static func mocked(url:String, title:String) -> LinkPreview{
        return LinkPreview(url:url, title:title)
    }
}

