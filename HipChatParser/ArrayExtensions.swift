//
//  ArrayExtensions.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 29.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation

extension Array{
    
    
    /// Function determine a randomized index based on 0..<self.count
    ///
    /// - Returns: Random index
    func randIndex() -> Int {
        return Int(arc4random_uniform(UInt32(self.count)))
    }
    
    
    /// Function returns random Element by using randIndex()
    ///
    /// - Returns: Random Element
    func randItem() -> Element{
        let index = self.randIndex()
        return self[index]
    }
}

