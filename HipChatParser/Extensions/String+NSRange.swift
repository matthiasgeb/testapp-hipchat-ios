//
//  StringExtension.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation

extension String {
    
    /// An `NSRange` that represents the full range of the string.
    var nsrange: NSRange {
        return NSRange(location: 0, length: utf16.count)
    }
    
    /// Returns a substring with the given `NSRange`,
    /// or `nil` if the range can't be converted.
    func substring(with nsrange: NSRange) -> String? {
        guard let range = nsrange.toRange()
            else { return nil }
        let start = UTF16Index(range.lowerBound)
        let end = UTF16Index(range.upperBound)
        return String(utf16[start..<end])
    }
}
