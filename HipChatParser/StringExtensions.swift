//
//  StringExtensions.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 28.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    
    /// Wrapper function for String(contentsOf: encoding:)
    /// Happens syncroniously, should never be called directly on Main Thread
    ///
    /// - Parameters:
    ///   - url: An URL for the ressource to be downloaded
    ///   - encoding: Encoding to be used to encode String with
    /// - Returns: Encoded String or nil
    static func from(url:URL, encoding:String.Encoding) -> String?{
        do {
            return try String(contentsOf: url, encoding: encoding)
        } catch let error {
            print("Error: \(error.localizedDescription)")
            return nil
        }
    }

    
    /// Returns a new string made by replacing all HTML-Entities e.g.:
    /// - '\&nbsp;' with ' ' (Space)
    /// - '\&amp;'  with '&'
    /// - '\&euro;' with '€'
    /// - '\&#33;'  with '!'
    /// - ... for more see: http://www.w3schools.com/html/html_entities.asp
    ///
    /// - Returns: String
    func htmlDecodedString() -> String {
        
        guard let data = self.data(using: self.smallestEncoding) else { return self }
        
        let attributedOptions: [String : Any] = [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                  NSCharacterEncodingDocumentAttribute: self.smallestEncoding.rawValue]
        do {
            let attributedString = try NSAttributedString(data: data, options: attributedOptions, documentAttributes: nil)
            return attributedString.string
        } catch {
            print("Error: \(error)")
            return self
        }
    }
    
    /// Returns a new string made by removing all characters after a given 
    /// character count and replaces them with given tail-string.
    func truncateCharacters(after count:Int, with tail:String) -> String{
        
        if self.characters.count > count{
            let index = self.index(self.startIndex, offsetBy: count-1)
            return self.substring(to: index) + tail
        }
        return self
    }
}
