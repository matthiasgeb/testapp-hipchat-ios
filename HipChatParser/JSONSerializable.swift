//
//  Serializable.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 28.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//
// Most of the implementation is based on the code in [this blog post](http://codelle.com/blog/2016/5/an-easy-way-to-convert-swift-structs-to-json/)

import Foundation

protocol JSONRepresentable {
    var JSONRepresentation: Any{ get }
}

protocol JSONSerializable: JSONRepresentable {
    
}

extension JSONSerializable {
    var JSONRepresentation: Any {
        var representation = [String: Any]()
        
        for case let (label?, value) in Mirror(reflecting: self).children {
            
            switch value {
                
            case let value as [JSONSerializable]:
                representation[label] = value.map({$0.JSONRepresentation})
            
            case let value as [String]:
                representation[label] = value.map({$0})
                
            case let value as String:
                representation[label] = value
                
            default:
                // Ignore any unserializable properties
                break
            }
        }
        
        return representation
    }
}

extension JSONSerializable {
    
    func toJSON(options:JSONSerialization.WritingOptions = []) -> String? {
        let representation = JSONRepresentation
        
        guard JSONSerialization.isValidJSONObject(representation) else { return nil }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: representation, options: options)
            let str =  String(data: data, encoding: .utf8)
            return str?.replacingOccurrences(of: "\\/", with: "/")
        } catch {
            return nil
        }
    }
}
