//
//  MessageComponents.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation


/// Regex Pattern for each message component which can be retreived by regex. 
/// Others components might be detected differently
///
/// - mention:  It always starts with an '@' and ends when hitting a non-word character. 
///             The first and only capture group holds the actual mention-name.
/// - emoticon: Alphanumeric strings, no longer than 15 characters, contained in parenthesis. 
///             The first and only capture group holds the actual emoticon-name.

enum MessageComponentsRegexPattern:String{
    case mention = "\\@(\\w+)"
    case emoticon = "\\(([a-z0-9]{1,15})\\)"    
}


/// JSON Serializable Object representation of Chat-Message-Components

struct MessageComponents: JSONSerializable{

    
    /// Array of 'mention' names e.g. ["peter", "john", "lisa"]
    /// default value = nil - JSONSerializer ignores nil-properties
    var mentions:[String]? = nil
    
    /// Array of 'emoticon'-names e.g. ["boom", "branch", "bitbucket"]
    /// default value = nil - JSONSerializer ignores nil-properties
    var emoticons:[String]? = nil
    
    /// Array of 'LinkPreview'-Objects
    /// default value = nil - JSONSerializer ignores nil-properties
    var links:[LinkPreview]? = nil
    
    /// Creates a new `MessageComponents`-Object by passing a `message:String`.
    /// The `message` will be used to detect:
    ///  - `mentions` - e.g. "@peter"
    ///  - `emoticons` - e.g. "(boom)"
    ///  - `links` - e.g. "http://twitter.com"
    
    /// - Parameter message: The actual message-string typed by the user in e.g. a chat.
    
    init(message:String) {
        if let mentionsMatches = StringParser.matches(in: message, for: MessageComponentsRegexPattern.mention.rawValue, withCaptureGroupIndex: 1), !mentionsMatches.isEmpty{
            self.mentions = mentionsMatches
        }
        
        if let emoticonsMatches = StringParser.matches(in: message, for: MessageComponentsRegexPattern.emoticon.rawValue, withCaptureGroupIndex: 1), !emoticonsMatches.isEmpty{
            self.emoticons = emoticonsMatches
        }
        
        if let linkMatches = StringParser.matches(in: message, for: .link), !linkMatches.isEmpty{
            self.links = linkMatches.map({ LinkPreview(linkString: $0) })
        }
    }
    
    /// Creates a new MessageComponents-Object setting the components explicitly 
    /// and bypassing the component detection. Can be used to create mocked MessageComponents objects.
    /// All parameters are optional and have a default value.
    ///
    /// - Parameters:
    ///   - mentions: Array of 'mention' names e.g. ["peter", "john", "lisa"]
    ///   - emoticons:  Array of 'emoticon'-names e.g. ["boom", "branch", "bitbucket"]
    ///   - links:  Array of 'LinkPreview'-Objects
    
    fileprivate init(mentions:[String]? = nil, emoticons:[String]? = nil, links:[LinkPreview]? = nil){
        
        self.mentions = mentions
        self.emoticons = emoticons
        self.links = links
    }
}

extension MessageComponents{
    
    
    /// Static function to create a mocked MessageComponents object while bypassing the component detection.
    ///
    /// - Parameters:
    ///   - mentions: Array of 'mention' names e.g. ["peter", "john", "lisa"]
    ///   - emoticons:  Array of 'emoticon'-names e.g. ["boom", "branch", "bitbucket"]
    ///   - links:  Array of 'LinkPreview'-Objects
    /// - Returns: MessageComponents-Object consisting of the components passed as parameter.
    
    static func mocked(mentions:[String]? = nil,
                       emoticons:[String]? = nil,
                       links:[LinkPreview]? = nil) -> MessageComponents{
        
        return MessageComponents(mentions: mentions,
                                 emoticons: emoticons,
                                 links: links)
    }
}
