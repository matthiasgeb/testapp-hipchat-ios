//
//  Parseable.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import Foundation

struct StringParser{
    
    
    /// Parses given String with regexPattern. 
    /// Regex is always excecuted with `.caseInsensitive`-option.
    /// Supports a single capture group for value capturing.
    ///
    /// - Parameters:
    ///   - string: `String` to be parsed
    ///   - pattern: regex `String` to parse with. Should provide capture groups syntax if `idx != 0`
    ///   - idx: capture group index that holds the expected value. 
    ///     Should not be greater than the amount of capture groups in regex.
    /// - Returns: `String`-matches or empty array. Returns nil in case of an error occcuring while excecuting regex
    static func matches(in string:String, for pattern:String, withCaptureGroupIndex idx:Int = 0 ) -> [String]?{
        
        do {
            let regex = try NSRegularExpression(pattern: pattern, options:[.caseInsensitive])
            let matches = regex.matches(in: string, range: string.nsrange)
            
            return matches.flatMap {
                if $0.numberOfRanges <= idx{
                    return nil
                }else{
                    return string.substring(with: $0.rangeAt(idx))
                }
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return nil
        }
    }
    
    /// Parses given String with NSDataDetector with given `NSTextCheckingResult.CheckingType`
    ///
    /// - Parameters:
    ///   - string: `String` to be parsed
    ///   - checkingTypes: The supported checking types are a subset of the types `NSTextCheckingResult.CheckingType`. ( date, address, link, phoneNumber and transitInformation)
    /// - Returns: `String`-matches or empty array. Returns nil in case of an error while CheckingType detection
    static func matches(in string:String, for checkingTypes: NSTextCheckingResult.CheckingType) -> [String]?{
        
        do {
            let linkDetector = try NSDataDetector(types:checkingTypes.rawValue)
            
            let matches = linkDetector.matches(in: string, options: .reportCompletion, range: string.nsrange)
            
            return matches.flatMap { string.substring(with: $0.range) }
            
        } catch let error as NSError{
            print("invalid regex: \(error.localizedDescription)")
            return nil
        }
    }
}
