//
//  ViewController.swift
//  HipChatParser
//
//  Created by Matthias Gebhardt on 27.12.16.
//  Copyright © 2016 Matthias Gebhardt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var outputTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var transformButton: UIButton!
    

    //MARK: - Properties
    
    // Instantiate sample message lazyly (only when used)
    private lazy var sampleMessages:[SampleMessage]? = SampleDataHelper.loadMessages()
   
    // Instantiate text-components lazyly
    private lazy var sampleMentions:[SampleMention]? = SampleDataHelper.loadMentions()
    private lazy var sampleEmoticons:[SampleEmoticon]? = SampleDataHelper.loadEmoticons()
    private lazy var sampleLinks:[SampleEmoticon]? = SampleDataHelper.loadLinks()
    
    
    /// OperationQueue for asynchronous Operations
    private let operationQueue = OperationQueue()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setRandomMessage()
    }

    // MARK: - IBActions
    
    /// Sets a random message as `inputTextView.text` (for convenience)
    /// Resets current UI, see `resetAction()`
    @IBAction func setRandomMessage(){
        resetAction()
        inputTextView.text = sampleMessages?.randItem()
        outputTextView.text = ""
    }

    /// Adds a random emoticon-fomatted strings to `inputTextView.text` (for convenience)
    @IBAction func addEmoticonAction() {
        addTextToInputTextView(text: sampleEmoticons?.randItem())
    }
    
    /// Adds a random mention-fomatted strings to `inputTextView.text` (for convenience)
    @IBAction func addMentionAction() {
        addTextToInputTextView(text: sampleMentions?.randItem())
    }
    
    /// Adds a random link-fomatted strings to `inputTextView.text` (for convenience)
    @IBAction func addLinkAction() {
        addTextToInputTextView(text: sampleLinks?.randItem())
    }
    
    /// Transforms `inputexView.text` to JSON, while it takes care of UI-states
    @IBAction func transformAction() {
    
        transformButton.isEnabled = false
        inputTextView.isEditable = false
        activityIndicator.startAnimating()
        
        let transformOp = MessageStringToJSONTransformOperation(input:inputTextView.text)
        
        transformOp.completionBlock = {
            
            if transformOp.isCancelled{
                return
            }
            
            DispatchQueue.main.async {
                
                self.outputTextView.text = transformOp.outputJSONString ?? "Sorry! Couldn\'t transform input"
                self.transformButton.isEnabled = true
                self.inputTextView.isEditable = true
                self.activityIndicator.stopAnimating()
            }
        }

        operationQueue.addOperation(transformOp)
    }
    
    /// Resets all UI and cancels current JSON transformation operations
    @IBAction func resetAction() {
        operationQueue.cancelAllOperations()
        activityIndicator.stopAnimating()
        inputTextView.text = ""
        outputTextView.text = ""
        transformButton.isEnabled = true
    }
}

extension ViewController{
    
    
    /// Adds a `text:String` to `inputTextView.text`
    /// Current cursor position and text selection will be considered.
    ///
    /// - Parameter text: Text to be added
    func addTextToInputTextView(text:String?){
        
        guard let text = text else { return }
        
        if let selectedRange = inputTextView.selectedTextRange{
           inputTextView.replace(selectedRange, withText: " " + text)
        }else{
            inputTextView.text.append(" " + text)
        }
        
        let bottom = NSMakeRange(inputTextView.text.characters.count, 1)
        inputTextView.scrollRangeToVisible(bottom)
    }
}
